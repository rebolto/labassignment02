//Noah Diplarakis
//#2244592
package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    public Bicycle(String manufacturered,int numberOfGears,double maximumSpeed){
        this.manufacturer=manufacturered;
        this.numberGears=numberOfGears;
        this.maxSpeed=maximumSpeed;
    }
    public String toString(){
        return "Manufacturer: "+this.manufacturer+", "+"Number of Gears: "+this.numberGears+", "+"MaxSpeed: "+this.maxSpeed;
    }
}