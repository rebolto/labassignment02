package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[]args){
        Bicycle[] bikeobjects=new Bicycle[4];
        bikeobjects[0]=new Bicycle("Yamaha",5,40);
        bikeobjects[1]=new Bicycle("Suzuki", 6, 55);
        bikeobjects[2]=new Bicycle("Mitsubishi", 7, 95);
        bikeobjects[3]=new Bicycle("R1", 4, 120);
        for(int i =0;i<bikeobjects.length;i++){
            System.out.println(bikeobjects[i]);
        }
    }
}
